using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[BurstCompile]
partial struct SpawnerSystem : ISystem
{ 
    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnDestroy(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        float deltaTime = SystemAPI.Time.DeltaTime;
        var ecbSingleton = SystemAPI.GetSingleton<EndInitializationEntityCommandBufferSystem.Singleton>();
        EntityCommandBuffer ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);
        SpawnerJob spawnerJob = new SpawnerJob
        {
            ECB = ecb,
            deltaTime = deltaTime
        };
        spawnerJob.Schedule();
    }
}


[BurstCompile]
partial struct SpawnerJob : IJobEntity
{
    public EntityCommandBuffer ECB;
    public float deltaTime;
    void Execute(ref SpawnerAspect spawner)
    {
        spawner.ElapseTime(deltaTime, ECB);
    }
}