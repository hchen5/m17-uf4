using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;



class SpeedAuthoring : MonoBehaviour
{
    public float speed;
    public Vector3 direction;

    class SpeedBaker : Baker<SpeedAuthoring>
    {
        public override void Bake(SpeedAuthoring authoring)
        {
            AddComponent(new Speed
            {
                speed = authoring.speed,
                direction = authoring.direction.normalized
            });
        }
    }
}
public struct Speed : IComponentData
{
    public float speed;
    public float3 direction;
}