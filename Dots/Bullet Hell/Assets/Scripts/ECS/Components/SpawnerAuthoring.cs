using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UIElements;

class SpawnerAuthoring : MonoBehaviour
{
    public GameObject prefab;
    public float spawnRate;
    public Vector3 m_transform;
    public Vector3 Player;

    [Header("Random")]
    public bool useSeed = false;
    public ushort seed = 1;

    class SpawnerBaker : Baker<SpawnerAuthoring>
    {
        public override void Bake(SpawnerAuthoring authoring)
        {

            AddComponent(new Spawner
            {
                entityPrefab = GetEntity(authoring.prefab),
                transformPosition = authoring.m_transform,
                direction = authoring.Player,
                spawnRate = authoring.spawnRate,
                elapsedTime = authoring.spawnRate,
                random = authoring.useSeed ?
                      new Unity.Mathematics.Random(authoring.seed)
                    : new Unity.Mathematics.Random((ushort)UnityEngine.Random.Range(0, 65536))
            }) ;
        }
    }
}
//Dades
public struct Spawner : IComponentData
{
    public Entity entityPrefab;
    public Vector3 transformPosition;
    public Vector3 direction;
    public float spawnRate;
    public float elapsedTime;
    public Unity.Mathematics.Random random;
}