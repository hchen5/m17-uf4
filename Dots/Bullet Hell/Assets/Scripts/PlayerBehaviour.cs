using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.UI;


public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_Text;    
    [SerializeField]
    private TextMeshProUGUI m_Text2;

    [SerializeField]
    private float m_Speed;
    private Rigidbody m_rigidBody;
    [SerializeField]
    public Transform PuntoDisparo;
    
    private static PlayerBehaviour m_Instance;
    public static PlayerBehaviour Instance => m_Instance;

    private Unity.Mathematics.float3 m_Position;
    public Unity.Mathematics.float3 Position => m_Position;

    private float m_RadiusSQ;
    public float RangeRadiusSQ => m_RadiusSQ;

    private Renderer renderer;
    private Material material;
    private int punts;
    void Awake()
    {
        renderer = gameObject.GetComponent<Renderer>();
        material = renderer.material;
        material.color = Color.red;

        if (m_Instance == null)
            m_Instance = this;
        else
            Destroy(gameObject);

        m_RadiusSQ = 1;
        m_Position = transform.position;
        
        m_rigidBody= GetComponent<Rigidbody>();
        UpdateRadius();
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        punts= 0;
    }
    private void Start()
    {
        m_Text2.enabled = false;
        m_Text.text = "Puntuaci�" +punts;
        StartCoroutine(changeColor());
    }

    // Update is called once per frame
    void Update()
    {
       
        if (Input.GetKey(KeyCode.A))
        {
            //canvia els Angles del personatge
            transform.eulerAngles = new Vector3(0, 180, 0);
            m_rigidBody.velocity = new Vector3(-m_Speed, m_rigidBody.velocity.y, 0);

        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            m_rigidBody.velocity = new Vector3(m_Speed, m_rigidBody.velocity.y, 0);

        }
        if (Input.GetKeyUp(KeyCode.D) && !Input.GetKeyDown(KeyCode.W) && !Input.GetKeyDown(KeyCode.S) && !Input.GetKeyDown(KeyCode.A))
        {

            m_rigidBody.velocity = new Vector3(0, 0, 0);

        }
        if (Input.GetKeyUp(KeyCode.A) && !Input.GetKeyUp(KeyCode.D) && !Input.GetKeyDown(KeyCode.W) && !Input.GetKeyDown(KeyCode.S))
        {
            m_rigidBody.velocity = new Vector3(0, 0, 0);

        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.eulerAngles = new Vector3(0, 180, 90);
            m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x, m_Speed, 0);

        }
        if (Input.GetKeyUp(KeyCode.W) && !Input.GetKeyDown(KeyCode.S) && !Input.GetKeyDown(KeyCode.A) && !Input.GetKeyUp(KeyCode.D))
        {

            m_rigidBody.velocity = new Vector3(0, 0, 0);

        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.eulerAngles = new Vector3(0, 180, -90);
            m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x, -m_Speed, 0);

        }

        if (Input.GetKeyUp(KeyCode.S) && !Input.GetKeyDown(KeyCode.A) && !Input.GetKeyUp(KeyCode.D) && !Input.GetKeyUp(KeyCode.W))
        {

            m_rigidBody.velocity = new Vector3(0, 0, 0);

        }
        m_Position= transform.position;
        EntityQuery inRangeEntitiesQuery = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(IsInRangeTag));
        Unity.Collections.NativeArray<Entity> inRangeEntities = inRangeEntitiesQuery.ToEntityArray(Unity.Collections.Allocator.Temp);

        Debug.Log(inRangeEntities.Length);
        foreach (Entity entity in inRangeEntities)
        {
            if (material.color == Color.red) {
                punts++;
                m_Text.text = "Puntuaci�: "  + punts;
                entityManager.DestroyEntity(entity);
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        inRangeEntities.Dispose();
        
    }
    IEnumerator changeColor() 
    {
        while (true)
        {
            if (material.color == Color.red)
                material.color = Color.green;
            else
                material.color = Color.red;

            int a = Random.Range(10, 20);
            yield return new WaitForSeconds(a);
            print("cambio");
            m_Text2.enabled = true;
            yield return new WaitForSeconds(3f);
            m_Text2.enabled= false;
        }
    }
    public EntityManager entityManager;
    
    private void UpdateRadius()
    {
        m_RadiusSQ = 1;
        Debug.Log(m_RadiusSQ);
    }

}
