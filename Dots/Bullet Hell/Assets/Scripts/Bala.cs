using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class Bala : MonoBehaviour
{
    private Rigidbody bola;
    public float Speed;

 

    private void Awake()
    {

        bola = GetComponent<Rigidbody>();

    }
   
    // Update is called once per frame
    void Update()
    {
        bola.velocity = transform.right * Speed;

    }
    private void OnBecameInvisible()
    {
        Destroy(gameObject, 2f);
    }

}
