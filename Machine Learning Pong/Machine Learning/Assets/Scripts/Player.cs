using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Integrations.Match3;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class Player : Agent
{
    public delegate void ResetSimulation();
    public event ResetSimulation OnResetSimulation;

    public delegate void Goal();
    public event Goal OnGoal;

    private Vector2 m_InitPos;
    private Rigidbody2D m_Rb;
    private CircleCollider2D m_CircleCollider;
    private float m_Speed = 4f;
    [SerializeField]
    private LayerMask m_RayMask;

    private void Awake()
    {
        m_InitPos= transform.position;
        m_Rb =GetComponent<Rigidbody2D>();
        m_CircleCollider= GetComponent<CircleCollider2D>();
    }

    private void ResetPlayer()
    {
        transform.position = m_InitPos;
        m_Rb.velocity = Vector2.zero;

        OnResetSimulation?.Invoke();
    }
    /*
    * Cada cop que es reinic�i la simulaci� 
    */
    public override void OnEpisodeBegin()
    {
        ResetPlayer();
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        Vector2 Top = new Vector2(transform.position.x, transform.position.y + m_CircleCollider.radius);
        Vector2 Bot = new Vector2(transform.position.x, transform.position.y - m_CircleCollider.radius);
        RaycastHit2D hit;
        float distance;

        sensor.AddObservation(transform.position.y - transform.parent.position.y);
        //print(transform.position.y - transform.parent.position.y);

        hit = Physics2D.Raycast(Bot, Vector2.right, 20f, m_RayMask);
        if(hit.collider)
            distance = hit.distance;
        else
            distance = 10f;
        sensor.AddObservation(distance);
        
        Debug.DrawLine(Bot, Bot + Vector2.right* distance, Color.black);
        
        hit = Physics2D.Raycast(Top, Vector2.right, 20f, m_RayMask);
        if(hit.collider)
            distance = hit.distance;
        else
            distance = 10f;
        sensor.AddObservation(distance);
        Debug.DrawLine(Top, Top + Vector2.right* distance, Color.yellow);
        
    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        int direction = actions.DiscreteActions[0] - 1;
        m_Rb.velocity = Vector2.up * direction * m_Speed;
    }
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        int direction = 1;
        if (Input.GetKey(KeyCode.S))
            direction -= 1;
        if (Input.GetKey(KeyCode.W))
            direction += 1;

        ActionSegment<int> actions = actionsOut.DiscreteActions;
        actions[0] = direction;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Obstacle")
        {
            //C�stig amb un valor negatiu
            AddReward(-10f);
            //I seria gameOver
            EndEpisode();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Goal")
        {
            OnGoal?.Invoke();
            //Donem un premi, considerablement menor al c�stig per tal
            //que no noti que li surt a compte estrellar-se
            AddReward(0.1f);
        }
    }
}
