using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUI : MonoBehaviour
{
    private TextMeshProUGUI m_Text;

    [SerializeField]
    private Manager m_Manager;

    void Awake()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
        m_Manager.OnGUI += UpdateScore;
    }

    private void UpdateScore()
    {
        m_Text.text = "score: " + m_Manager.Score;
    }
}
