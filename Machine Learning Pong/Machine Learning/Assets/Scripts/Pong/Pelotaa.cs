using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Pelotaa : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;
    private Rigidbody2D m_Rb;

    private void Awake()
    {
        m_Rb = GetComponent<Rigidbody2D>();
        
    }

    // Start is called before the first frame update
    void Start()
    {
        StartBall();
    }

    public void StartBall()
    {
        m_Rb.velocity = Vector2.zero;
        this.transform.localPosition = Vector3.zero;
        float x = Random.Range(0, 2) == 0 ? -1 : 1;
        float y = Random.Range(0, 2) == 0 ? -1 : 1;
        m_Rb.velocity = new Vector2( x,  y) *m_Speed;
    }
}
