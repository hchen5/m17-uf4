using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fora : MonoBehaviour
{
    public delegate void Forapelota();
    public event Forapelota onForapelota;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ball") 
        {
            //PaletaPlayer.FindObjectOfType<PaletaPlayer>().GameOver(-10f);
            onForapelota ?.Invoke();
        }
    }
 
}
