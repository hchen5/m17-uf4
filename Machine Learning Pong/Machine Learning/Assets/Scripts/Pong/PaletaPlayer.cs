using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEditor.Timeline.Actions;
using UnityEngine;

public class PaletaPlayer : Agent
{
    [SerializeField]
    private Fora m_Fora;
    [SerializeField]
    private Transform m_BallTransform;
    private Vector2 m_InitPos;
    private Rigidbody2D m_Rb;
    [SerializeField]
    private float m_Speed;

    private void Awake()
    {
        m_Fora.onForapelota += GameOver;
        m_InitPos = transform.position;
        m_Rb = GetComponent<Rigidbody2D>();

    }
    
    private void ResetPlayer()
    {
        transform.position = m_InitPos;
        //OnResetSimulation?.Invoke();
    }
    /*
    * Cada cop que es reinic�i la simulaci� 
    */
    public override void OnEpisodeBegin()
    {
        ResetPlayer();
        //m_BallTransform.localPosition = Vector3.zero;
        m_BallTransform.GetComponent<Pelotaa>().StartBall();
    }
    public override void CollectObservations(VectorSensor sensor)
    {
        float position = m_BallTransform.localPosition.x;
        sensor.AddObservation(m_BallTransform.localPosition.x);
        sensor.AddObservation(m_BallTransform.localPosition.y);
        sensor.AddObservation(m_BallTransform.GetComponent<Rigidbody2D>().velocity);
        sensor.AddObservation(transform.localPosition.y);

    }
    
    public override void OnActionReceived(ActionBuffers actions)
    {
        int direction = actions.DiscreteActions[0] - 1;
        //Debug.Log(direction);
        m_Rb.velocity = Vector2.up * direction * m_Speed;
    }
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        int direction = 1;
        if (Input.GetKey(KeyCode.S))
            direction -= 1;
        if (Input.GetKey(KeyCode.W))
            direction += 1;

        ActionSegment<int> actions = actionsOut.DiscreteActions;
        actions[0] = direction;
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ball")
        {
            
            AddReward(0.1f);
            //print("Tocado + 0.1");
            
        }
    }
    public void GameOver()
    {
        //C�stig amb un valor negatiu
        AddReward(-10f);
        //print("F");
        //I seria gameOver
        EndEpisode();
    }
}
