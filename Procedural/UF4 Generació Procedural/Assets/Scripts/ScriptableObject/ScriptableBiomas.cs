using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "ScriptableBiomas", menuName = "ScriptableObjects/ScriptableBiomas")]
public class ScriptableBiomas : ScriptableObject
{
    [SerializeField]
    public float m_frequency;
    [SerializeField]
    public int m_Amplitude;
    [SerializeField]
    public float m_CaveFrequency;
    [SerializeField]
    public float m_GoldFrequency;
    [SerializeField]
    public Tile m_Top;
    [SerializeField]
    public Tile m_Mid;
    [SerializeField]
    public Tile m_Bot;
}
