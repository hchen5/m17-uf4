using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.Tilemaps;

public class Procedural : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField]
    private float m_Seed;
    [SerializeField]
    private int m_Width;
    [SerializeField]
    private int m_Height;
    [SerializeField]
    private float m_Scale;
    [SerializeField]
    private float m_Frequency;
    [SerializeField]
    private float m_CaveFrecuency;
    [SerializeField]
    private Tile m_BoxTile;
    [SerializeField]
    private float m_BoxFrecuency;
    [Range(0f, 1f)]
    [SerializeField]
    private float m_BoxThreshold;
    [Range(0f, 1f)]
    [SerializeField]
    private float m_CaveThreshold;
    [SerializeField]
    private int m_Amplitude;
    [Header("Tiles x Cord")]
    [SerializeField]
    private Tile m_Gold;
    [SerializeField]
    private float m_GoldFrequency;
    [SerializeField]
    private float m_GoldThreshold;
    [SerializeField]
    public float m_DirtCord;
    public float m_ClayCord;
    public float m_StoneCord;
    [Header("Tiles")]
    [SerializeField]
    private Tilemap m_Tilemap;
    [Header("Meteorite Biome")]
    [SerializeField]
    ScriptableBiomas m_SOMeteorite;
    [Header("Ice Biome")]
    [SerializeField]
    ScriptableBiomas m_SOIce;
    [Header("Normal")]
    [SerializeField]
    ScriptableBiomas m_SONormal;
    private int[] Heights;
    [Header("Biome")]
    [SerializeField]
    private float m_BiomeScale;
    [SerializeField]
    private float m_BiomeFrequency;
    [SerializeField]
    private float m_BiomeThreshold;
    [SerializeField]
    private float m_MeteoriteThreshold;
    private Vector3Int[] m_CoordCave;
    private const int MAX_OCTAVES = 8;
    [SerializeField]
    [Range(0, MAX_OCTAVES)]
    private int m_Octaves = 0;
    [Range(2, 3)]
    [SerializeField]
    private int m_Lacunarity = 2;
    [SerializeField]
    [Range(0.1f, 0.9f)]
    private float m_Persistence = 0.5f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            m_Tilemap.ClearAllTiles();
            //Generate();
            Biomas();
            //GenerateCave();
            //GenerateGold();
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            m_Tilemap.ClearAllTiles();
            Biomas();
        }
    }

    private void Biomas() 
    {

        Heights = new int[m_Width];
        float xCord;
        float yCord;
        int y = 01;
        yCord = m_Seed + (y / m_Scale) * m_Frequency;
        for (int x = 0; x < m_Width; x++)
        {
            xCord = m_Seed + (x / m_Scale) * m_Frequency;
            float sample = Mathf.PerlinNoise(xCord, yCord);
            //sample me dice en qu� bioma estoy
            //float biomeFrequency, biomeAmplitude;
            ScriptableBiomas bioma;

            if (sample >= m_BiomeThreshold) {
                //quiere decir bioma 1
                bioma = m_SOIce;
            }
            else if(sample<= m_BiomeThreshold && sample>=m_MeteoriteThreshold)
            {
                //quiere decir bioma 2
                bioma = m_SOMeteorite;
            }
            else
            {
                bioma = m_SONormal;
            }
            for (int octave = 1; octave <= m_Octaves; octave++)
            {
                float newFreq = m_Frequency * m_Lacunarity * octave;
                float xOctaveCoord = m_Seed + (x / m_Scale) * newFreq;
                float yOctaveCoord = m_Seed + (y / m_Scale) * newFreq;

                //valor base de l'octava
                float octaveSample = Mathf.PerlinNoise(xOctaveCoord, yOctaveCoord);
                octaveSample = (octaveSample - .5f) * (m_Persistence / octave);
                sample += octaveSample;

               
            }

            float biomeX = m_Seed + (y / m_Scale) * bioma.m_frequency;
            float biomeY = m_Seed + (x / m_Scale) * bioma.m_frequency;
            float sample1 = Mathf.PerlinNoise(biomeX, biomeY);
            int height = (int)(sample1 * bioma.m_Amplitude);
            Heights[x] = height;
            GenerateGold();
            for (int i = 0; i < height; i++)
            {
                //if (sample1 > m_MeteoriteThreshold)
                m_Tilemap.SetTile(new Vector3Int(x, i, 0), SelectBiomeTile(i, bioma));

            }
        }
        GenerateCave();
    }

    private Tile SelectBiomeTile(int f, ScriptableBiomas bioma)
    {
        if (f >= (m_DirtCord) * m_Amplitude)
        {
            return bioma.m_Top;
        }
        else if (f > (m_ClayCord * m_Amplitude) && f < (m_DirtCord * m_Amplitude))
        {
            return bioma.m_Mid;
        }
        else
            return bioma.m_Bot;
    }
    private void GenerateBox()
    {
        float xCord;
        float yCord;
        foreach (Vector3Int coord in m_CoordCave)
        {
            yCord = m_Seed + (coord.y / m_Scale) * m_BoxFrecuency;
            xCord = m_Seed + (coord.x / m_Scale) * m_BoxFrecuency;
            float sample = Mathf.PerlinNoise(xCord, yCord);

            if (sample >= m_BoxThreshold)
                m_Tilemap.SetTile(coord, m_BoxTile);
        }
    }
    private void GenerateCave()
    {
        List<Vector3Int> CoordCaveList = new List<Vector3Int>();
        float xCord;
        float yCord;
        for (int y = 0; y < m_Height; y++)
        {
            //int y = 01;
            yCord = m_Seed + (y / m_Scale) * m_CaveFrecuency;
            for (int x = 0; x < m_Width; x++)
            {
                xCord = m_Seed + (x / m_Scale) * m_CaveFrecuency;
                float sample = Mathf.PerlinNoise(xCord, yCord);
                //int height = (int)(sample * m_Amplitude);
                //Heights[x] = height;

                for (int octave = 1; octave <= m_Octaves; octave++) 
                {
                    float newFreq = m_Frequency * m_Lacunarity * octave;
                    float xOctaveCoord = m_Seed + (x / m_Scale) * newFreq;
                    float yOctaveCoord = m_Seed + (y / m_Scale) * newFreq;

                    //valor base de l'octava
                    float octaveSample = Mathf.PerlinNoise(xOctaveCoord, yOctaveCoord);
                    octaveSample = (octaveSample - .5f) * (m_Persistence / octave);
                    sample += octaveSample;

                    if (Heights[x] >= y)
                    {
                        if (sample > m_CaveThreshold)
                        {
                            Vector3Int coord = new Vector3Int(x, y);
                            CoordCaveList.Add(coord);
                            m_Tilemap.SetTile(coord, null);
                        }


                    }
                }

               
                //m_Tilemap.SetTile(new Vector3Int(x, y, 0), Tilem(i));


            }

        }
        m_CoordCave = CoordCaveList.ToArray();

        GenerateBox();

    }
    //Depenent del valor que ens doni el PerlinNoise agafem un tile o un altre

    private void GenerateGold() 
    {
        float xCord;
        float yCord;
        for (int y = 0; y < m_Height; y++)
        {
            //int y = 01;
            yCord = m_Seed + (y / m_Scale) * m_GoldFrequency;
            for (int x = 0; x < m_Width; x++)
            {
                xCord = m_Seed + (x / m_Scale) * m_GoldFrequency;
                float sample = Mathf.PerlinNoise(xCord, yCord);
                //int height = (int)(sample * m_Amplitude);
                //Heights[x] = height;

                if (Heights[x] >= y*2)
                {
                    if (sample > m_GoldThreshold)
                    {
                        Vector3Int coord = new Vector3Int(x, y);
                        //CoordCaveList.Add(coord);
                        m_Tilemap.SetTile(coord, m_Gold);
                    }


                }
                //m_Tilemap.SetTile(new Vector3Int(x, y, 0), Tilem(i));


            }

        }
    }
    private void GenerateIron()
    {

    }
    private void GenerateMeteorite()
    {

    }
    /*
private void Generate() 
{
    Heights = new int[m_Width];
    float xCord;
    float yCord;
    //for (int y = 0; y < m_Height; y++) 
    //{
    int y = 01;
        yCord = m_Seed + (y / m_Scale) * m_Frequency;
        for (int x = 0; x < m_Width; x++)
        {
            xCord = m_Seed + (x /m_Scale) *m_Frequency;
            float sample = Mathf.PerlinNoise(xCord,yCord);
            int height = (int)(sample * m_Amplitude);
            Heights[x] = height; 
            for (int i = 0; i < height; i++)
                m_Tilemap.SetTile(new Vector3Int(x, i, 0), Tilem(i));
        }

    //}
}*/

    /*
    private void GenerateCave() 
    {
        List<Vector3Int> CoordCaveList = new List<Vector3Int>();
        float xCord;
        float yCord;
        for (int y = 0; y < m_Amplitude; y++) 
        {
        //int y = 01;
        yCord = m_Seed + (y / m_Scale) * m_CaveFrequency;
            for (int x = 0; x < m_Width; x++)
            {
                xCord = m_Seed + (x / m_Scale) * m_CaveFrequency;
                float sample = Mathf.PerlinNoise(xCord, yCord);
                for (int a=0; a < Heights.Length;a++) 
                {
                    if (Heights[x] >= y)
                    {
                        if (sample>0.62f)
                        {
                            Vector3Int coord = new Vector3Int(x, y);
                            CoordCaveList.Add(coord);
                            m_Tilemap.SetTile(new Vector3Int(x, y, 0), null);
                        }          
                    }
                }
            }
        }
        m_CoordCave = CoordCaveList.ToArray();
        GenerateBox();
    }*/


}
